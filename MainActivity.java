package com.proyecto.andriusic.yummy;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.view.Menu;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.software.shell.fab.ActionButton;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import io.codetail.animation.*;
import io.codetail.animation.ViewAnimationUtils;

public class MainActivity extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    String TAG = MainActivity.class.getSimpleName();
    public static String PACKAGE_NAME;
    private GoogleApiClient mGoogleApiClient;
    GoogleMap googleMap;
    GoogleMapOptions options = new GoogleMapOptions().liteMode(true);
    double longitude,latitude;
    final LatLng SantoDgo = new LatLng(18.4638432,-69.9270076);
    private LocationManager locationManager;
    public Menu mMenu;
    public ComponentName SEARCHABLE_ACTIVITY = new ComponentName("com.proyecto.andriusic.yummy","com.proyecto.andriusic.yummy.MainActivity");
    private SlidingUpPanelLayout mPanel;
    private LinearLayout mSlidingLayout;
    protected RecyclerView mRecyclerView;
    protected SearchCardAdapter mAdapter;
    public List<SearchCard> list;
    public final Context CONTEXT = this;
    public List<ListaRestaurantes> listaRestaurantes;
    ActionButton locationActionButton, placesActionButton;
    public SupportAnimator animator;
    RelativeLayout searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Enable Local Datastore.
        //Parse.enableLocalDatastore(this);
        Parse.initialize(this, "mmGisZ0gk7jZWb27yQuYJQxRqspFJcw8r5PvF4ZT", "6gl3c90uFMwv1vlKBBNrkEYQJmZJDQCQBlJKkuFA");
        toolbar_properties();
        createMapView();
        buildGoogleApiClient();
        searchButtonToolbar();
        slidingPanel_properties();
        cargarRestaurantes();
        PACKAGE_NAME = getApplicationContext().getPackageName();
         this.locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        final Location mLastLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        searchView = (RelativeLayout) findViewById(R.id.hiddenSearchBar);
        searchView.setVisibility(View.INVISIBLE);
        setStatusBarColor();
    }
    private void cargarRestaurantes(){

        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Restaurantes")
                .whereNotEqualTo("localizacion", null);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                listaRestaurantes = new ArrayList<ListaRestaurantes>();
                ListaRestaurantes lr = new ListaRestaurantes();
                if (e == null) {
                    for (int a = 0; a < parseObjects.size(); a++) {
                        lr = new ListaRestaurantes();
                        ParseObject object = parseObjects.get(a);
                        ParseGeoPoint location = (ParseGeoPoint) object.get("localizacion");
                        String nombre = (String) object.get("nombre");
                        lr.setLocationRestaurante(location);
                        lr.setNombreRestaurante(nombre);
                        listaRestaurantes.add(lr);
                    }
                    for (int a = 0; a < listaRestaurantes.size(); a++) {
                        addRestaurantMarker(listaRestaurantes.get(a).getLocationRestaurante(), listaRestaurantes.get(a).getNombreRestaurante());
                    }

                } else {
                    System.out.println("Error" + e);
                }
            }
        });


    }
    private void toolbar_properties(){
        Toolbar tb = (Toolbar) findViewById(R.id.main_toolbar);
        tb.setContentInsetsAbsolute(0,0);
        setSupportActionBar(tb);

        MenuItem item = tb.getMenu().findItem(R.id.action_settings);
        if (item != null)
            item.setVisible(false);

        com.rey.material.widget.EditText sb = (com.rey.material.widget.EditText) findViewById(R.id.editText2);
        sb.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    return true;
                }
                return false;
            }
        });
    }
    private void slidingPanel_properties(){
        mPanel = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mPanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        mSlidingLayout = (LinearLayout) findViewById(R.id.dragView);
        mSlidingLayout.setVisibility(View.GONE);
    }
    private void searchButtonToolbar(){
        ImageButton searchButton = (ImageButton) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSearchBar();
            }
        });
    }

    private Location getLocation(){
        Location mLastLocation;
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(mLastLocation != null){
           this.latitude = mLastLocation.getLatitude();
           this.longitude = mLastLocation.getLongitude();
        }
        return mLastLocation;
}
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }
    private void createMapView(){

    try{
        if(null == googleMap){
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SantoDgo,12));
            if(null == googleMap){
                Toast.makeText(getApplicationContext(), "Error cargando el mapa", Toast.LENGTH_LONG).show();
            }
        }
    }catch(NullPointerException exception){
        Log.e("Yummy", exception.toString());
        }
    }

    private void addRestaurantMarker(ParseGeoPoint location, String nombre){
        if(null != googleMap){
            googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.getLatitude(), location.getLongitude()))
                            .title(nombre)
                            .draggable(false)

                    // .icon(BitmapDescriptorFactory.fromResource())
            );
        }
    }

    private void addMarker(Location location){
        if(null != googleMap){
            googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.getLatitude(), location.getLongitude()))
                            .title("Yo")
                            .draggable(false)
            );
        }
    }
    private void addCircle(Location location){
        if(placesActionButton.isHidden()){
        Circle circle = googleMap.addCircle(new CircleOptions()
                .center(new LatLng(location.getLatitude(), location.getLongitude()))
                .radius(500)
                .strokeColor(Color.parseColor("#E84343"))
                .strokeWidth(2)
                .fillColor(Color.parseColor("#4DE84343")));
    }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

                /*switch(item.getItemId()){
                    case R.id.action_favorite:
                                 return true;
                        }*/
                 return super.onOptionsItemSelected(item);
                }

    @Override
    public void onConnected(Bundle bundle) {

        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        locationActionButton = (ActionButton) findViewById(R.id.action_button_location);
        locationActionButton.setButtonColor(getResources().getColor(R.color.fab_material_red_500));
        locationActionButton.setButtonColorPressed(getResources().getColor(R.color.fab_material_red_900));
        locationActionButton.setImageResource(R.drawable.ic_place_white_48dp);
        locationActionButton.setImageSize(34.0f);
        locationActionButton.setShadowColor(getResources().getColor(R.color.fab_material_red_900));
        locationActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CONTEXT, "Boton Presionado", Toast.LENGTH_SHORT).show();
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                        || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    LocationListener locationListener = new MyLocationListener();
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);

                    Location mLastLocation, mLastLocationGPS;
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    mLastLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    if (mLastLocation != null) {
                        addMarker(mLastLocation);
                        addCircle(mLastLocation);
                        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15), 3000, null);
                        placesActionButtonProperties();
                        placesActionButton.hide();
                        placesActionButton.setVisibility(View.VISIBLE);
                        placesActionButton.setShowAnimation(ActionButton.Animations.SCALE_UP);
                        placesActionButton.show();
                    } else {
                        if (mLastLocationGPS != null) {
                            addMarker(mLastLocationGPS);
                            addCircle(mLastLocation);
                            LatLng latLng = new LatLng(mLastLocationGPS.getLatitude(), mLastLocationGPS.getLongitude());
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15), 3000, null);
                            placesActionButtonProperties();
                            placesActionButton.hide();
                            placesActionButton.setVisibility(View.VISIBLE);
                            placesActionButton.setShowAnimation(ActionButton.Animations.SCALE_UP);
                            placesActionButton.show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Ubicacion no disponible", Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(CONTEXT);
                    dialog.setTitle(CONTEXT.getResources().getString(R.string.locaton_disabled_title));
                    dialog.setMessage(CONTEXT.getResources().getString(R.string.location_not_enabled));
                    dialog.setPositiveButton(CONTEXT.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            CONTEXT.startActivity(intent);
                        }
                    });
                    dialog.setNegativeButton(CONTEXT.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    dialog.show();
                }
            }
        });
        locationActionButton.show();
        placesActionButtonProperties();
        placesActionButton.hide();


    }
    public void placesActionButtonProperties(){

        placesActionButton = (ActionButton) findViewById(R.id.action_button_list);
        placesActionButton.setButtonColor(getResources().getColor(R.color.fab_material_red_500));
        placesActionButton.setButtonColorPressed(getResources().getColor(R.color.fab_material_red_900));
        placesActionButton.setImageResource(R.drawable.ic_list_white_48dp);
        placesActionButton.setImageSize(34.0f);
        placesActionButton.setShadowColor(getResources().getColor(R.color.fab_material_red_900));
        placesActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placesActionButton_action();
            }
        });

    }
    public void placesActionButton_action(){
         mRecyclerView = (RecyclerView)findViewById(R.id.recyclerNplaces);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(CONTEXT));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Restaurantes");
        parseQuery.whereNotEqualTo("localizacion", null);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {

                SearchCard sc;
                if (list == null) {
                    list = new ArrayList<SearchCard>();

                    Log.d("", "Creando lista en SlidingLayout");
                    for (int i = 0; i < parseObjects.size(); i++) {
                        sc = new SearchCard();

                        ParseObject object = parseObjects.get(i);
                        ParseGeoPoint parseGeoPoint, distancePrueba = new ParseGeoPoint();
                        Location mLastLocation, mLastLocationGPS;
                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        mLastLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (mLastLocation != null) {
                            distancePrueba.setLatitude(mLastLocation.getLatitude());
                            distancePrueba.setLongitude(mLastLocation.getLongitude());
                        } else {
                            if (mLastLocationGPS != null) {
                                distancePrueba.setLatitude(mLastLocationGPS.getLatitude());
                                distancePrueba.setLongitude(mLastLocationGPS.getLongitude());
                            }
                        }
                        parseGeoPoint = (ParseGeoPoint) object.get("localizacion");
                        double distance1 = parseGeoPoint.distanceInKilometersTo(distancePrueba);
                        if (distance1 < 0.500) {
                            Log.d("", (String) object.get("nombre"));
                            Log.d("", String.valueOf(object.get("descripcion")));
                            sc.setNombreRestaurante(String.valueOf(object.get("nombre")));
                            sc.setDescripcionRestaurante(String.valueOf(object.get("descripcion")));
                            String unidad;
                            if (distance1 < 1.00) {
                                distance1 = distance1 * 1000;
                                unidad = " Metros";
                            } else {
                                unidad = " Km";
                            }

                            DecimalFormat df = new DecimalFormat("0");
                            sc.setDistanciaRestaurante(String.valueOf(df.format(distance1) + unidad));
                            ParseFile file = (ParseFile) object.get("logo");
                            if (file != null) {
                                sc.setLogoRestaurante(file.getUrl());
                            } else {
                                sc.setLogoRestaurante("logo");
                            }
                            list.add(sc);
                        }


                    }

                    Collections.sort(list, SearchCard.SortDistance);
                    mAdapter = new SearchCardAdapter(list, R.layout.result_card, CONTEXT);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                    TextView textView = (TextView) findViewById(R.id.NRText);
                    TextView textViewNum = (TextView) findViewById(R.id.NRTextNum);

                    if (list.size() != 1) {
                        textView.setText("Restaurantes");
                        textViewNum.setText(+list.size() + "");
                    } else {
                        textView.setText("Restaurante");
                        textViewNum.setText(+list.size() + "");
                    }
                }
            }
        });
        toggleSlidingLayout();
    }
    public void setStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(R.color.rojoDark));
            window.setNavigationBarColor(this.getResources().getColor(R.color.rojo));
        }

    }
    public void performSearch(String text){
        Intent intent = new Intent(this, com.proyecto.andriusic.yummy.OpenSearchActivity.class);
        intent.putExtra("item", text);
        startActivity(intent);
    }
    private void toggleSearchBar(){
        int cx = (searchView.getLeft() + searchView.getRight());
        int cy = searchView.getTop();
        int radius = Math.max(searchView.getWidth(), searchView.getHeight());
        final com.rey.material.widget.EditText searchText = (com.rey.material.widget.EditText) findViewById(R.id.editText2);
        com.rey.material.widget.Button button = (com.rey.material.widget.Button) findViewById(R.id.imageButton2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleSearchBar();
            }
        });
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            if (searchView.getVisibility() == View.INVISIBLE){
                //Reveals searchview >= API v21
                SupportAnimator animator =
                        ViewAnimationUtils.createCircularReveal(searchView, cx, cy, 0, radius);
                animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animator.setDuration(400);
                searchView.setVisibility(View.VISIBLE);
                animator.addListener(new SupportAnimator.AnimatorListener() {
                    @Override
                    public void onAnimationStart() {

                    }

                    @Override
                    public void onAnimationEnd() {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);
                        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                                if (i == EditorInfo.IME_ACTION_SEARCH){
                                    performSearch(searchText.getText().toString());
                                    toggleSearchBar();
                                    return true;
                                }
                                return false;
                            }
                        });
                    }

                    @Override
                    public void onAnimationCancel() {

                    }

                    @Override
                    public void onAnimationRepeat() {

                    }
                });
                animator.start();

            }else{
                //Hides searchview >= API v21
                SupportAnimator animator =
                        ViewAnimationUtils.createCircularReveal(searchView, cx, cy, 0, radius);
                SupportAnimator reverseAnimator = animator.reverse();
                reverseAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                reverseAnimator.setDuration(400);

                reverseAnimator.addListener(new SupportAnimator.AnimatorListener() {
                    @Override
                    public void onAnimationStart() {

                    }

                    @Override
                    public void onAnimationEnd() {
                        searchView.setVisibility(View.INVISIBLE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
                        searchText.setText("");

                    }

                    @Override
                    public void onAnimationCancel() {

                    }

                    @Override
                    public void onAnimationRepeat() {

                    }
                });
                reverseAnimator.start();

            }
        }
    else{
            if (searchView.getVisibility() == View.INVISIBLE){
                //Reveals searchview < API v21
     Animator animation =
             android.view.ViewAnimationUtils.createCircularReveal(searchView, cx, cy, 0, radius);
            animation.setDuration(400);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
                animation.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        Log.d("TAG", "Animation ended");
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        searchView.setVisibility(View.VISIBLE);
                        Log.d("TAG", "Animation started");
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);
                        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                                if (i == EditorInfo.IME_ACTION_SEARCH) {
                                    performSearch(searchText.getText().toString());
                                    toggleSearchBar();
                                    return true;
                                }
                                return false;
                            }
                        });
                    }
                });
                animation.start();
        }else{
                //Hides searchview < API v21
                Animator animation =
                        android.view.ViewAnimationUtils.createCircularReveal(searchView, cx, cy, 0, radius);
                animation.setDuration(400);
                animation.setInterpolator(new ReverseInterpolator());
                animation.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        searchView.setVisibility(View.INVISIBLE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
                        searchText.setText("");
                    }
                });
                animation.start();
            }

        }
    }
    private void toggleSlidingLayout(){
        if (mSlidingLayout.getVisibility() == View.VISIBLE){

            mPanel.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            mSlidingLayout.setVisibility(View.GONE);
            Log.d("INFO: ", "Panel ocultado");
            /*if (locationActionButton.isShown() != true){
                locationActionButton.setShowAnimation(ActionButton.Animations.SCALE_UP);
                locationActionButton.show();
                placesActionButton.setShowAnimation(ActionButton.Animations.SCALE_UP);
                placesActionButton.show();
            }*/
        }else{
            mSlidingLayout.setVisibility(View.VISIBLE);
            mPanel.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            Log.d("INFO: ", "Panel mostrado");
            /*if (locationActionButton.isShown()){
                locationActionButton.setHideAnimation(ActionButton.Animations.SCALE_DOWN);
                locationActionButton.hide();
                placesActionButton.setHideAnimation(ActionButton.Animations.SCALE_DOWN);
                placesActionButton.hide();
            }*/

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        /*if (searchView.getVisibility() == View.VISIBLE){
            toggleSearchBar();
        }else{
            super.onBackPressed();
        }*/
        if (placesActionButton.getVisibility() == View.VISIBLE){
            mPanel.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            placesActionButton.hide();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SantoDgo, 12));

        }else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    private class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc){
            addMarker(loc);
            String longitude = "Longitude: " + loc.getLongitude();
            Log.v(TAG, longitude);
            String latitude = "Latitude: " + loc.getLatitude();
            Log.v(TAG, latitude);

    }
        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
}
    public class ReverseInterpolator implements Interpolator {
        @Override
        public float getInterpolation(float paramFloat) {
            return Math.abs(paramFloat -1f);
        }
    }
}

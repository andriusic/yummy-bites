package com.proyecto.andriusic.yummy;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class OpenSearchActivity extends ActionBarActivity {
    private RecyclerView mRecyclerView;
    private SearchCardAdapter mAdapter;
    public List<SearchCard> list;
    public final Context CONTEXT = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_search);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
        probarQuery(getIntent());
        verificarLista(list,"Al principio");
        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerLista);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(CONTEXT));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //if (Intent.ACTION_SEARCH.equals(getIntent())){
            Log.d("","Hola");
            String query = getIntent().getStringExtra("item");

            query = query.substring(0).toUpperCase();
            ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Restaurantes");
            parseQuery.whereStartsWith("nombre", query);

            parseQuery.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                public void done(List<ParseObject> parseObjects, ParseException e) {

                        SearchCard sc;
                        if (list == null) {
                            list = new ArrayList<SearchCard>();

                            Log.d("", "Creando lista");
                            for (int i = 0; i < parseObjects.size(); i++) {
                                sc = new SearchCard();
                                ParseObject object = parseObjects.get(i);
                                Log.d("", (String) object.get("nombre"));
                                String nombre = String.valueOf(object.get("nombre"));
                                Log.d("", String.valueOf(object.get("descripcion")));
                                sc.setNombreRestaurante(String.valueOf(object.get("nombre")));
                                sc.setDescripcionRestaurante(String.valueOf(object.get("descripcion")));
                                ParseFile file = (ParseFile) object.get("logo");
                                if (file != null) {
                                    sc.setLogoRestaurante(file.getUrl());
                                }else{
                                    sc.setLogoRestaurante("logo");
                                }
                                list.add(sc);
                            }
                            verificarLista(list, "Dentro de funcion Parse");
                            mAdapter = new SearchCardAdapter(list, R.layout.result_card, CONTEXT);
                            mRecyclerView.setAdapter(mAdapter);
                            ItemClickSupport ics = ItemClickSupport.addTo(mRecyclerView);
                            ics.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                                @Override
                                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                                    SearchCard currentSc = list.get(position);
                                    String nombre = currentSc.getNombreRestaurante();
                                    Intent intent = new Intent("android.intent.action.Restaurante");
                                    intent.putExtra("id", nombre);
                                    startActivity(intent);
                                }
                            });
                            mAdapter.notifyDataSetChanged();

                        }
                }
            });

       // }else{
       // Log.e("Error","Intent no concuerda");
       // }
       }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_open_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
    }



    private void probarQuery(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("El query es", query);
        }
    }
    private void verificarLista(List<SearchCard> listaFinal, String lugar) {
        if(listaFinal == null){
            System.out.println("Lista null: "+lugar);
        }else{
            System.out.println("La lista tiene: " +listaFinal.size() +" elementos: "+lugar);        }
    }
}
